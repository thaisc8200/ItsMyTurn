package com.itb.itsmyturn.ui.classes;

import com.itb.itsmyturn.model.SubjectsList;
import com.itb.itsmyturn.repository.Repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

public class ClassesViewModel extends ViewModel {
    private Repository repository = new Repository();

    public LiveData<SubjectsList> getClassesByTeacher(String email){
        return repository.getClasseByTeacher(email);
    }

    public LiveData<SubjectsList> getClassesByStudent(String email){
        return repository.getClasseByStudent(email);
    }

    public LiveData<Boolean> isTeacher(String email){
        return repository.isTeacher(email);
    }

}
