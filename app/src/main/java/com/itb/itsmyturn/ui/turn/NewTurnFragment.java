package com.itb.itsmyturn.ui.turn;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import com.itb.itsmyturn.R;
import com.itb.itsmyturn.model.TurnResponse;

public class NewTurnFragment extends Fragment {

    private NewTurnViewModel mViewModel;
    private MaterialButton btnSetTurn;
    private TextInputLayout tilTurnDescription;
    private String email;
    private int idSubject;
    private int idTurn;
    private boolean isReadOnly;

    public static NewTurnFragment newInstance() {
        return new NewTurnFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.new_turn_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        email = NewTurnFragmentArgs.fromBundle(getArguments()).getEmail();
        idSubject = NewTurnFragmentArgs.fromBundle(getArguments()).getIdSubject();
        idTurn = NewTurnFragmentArgs.fromBundle(getArguments()).getIdTurn();
        isReadOnly = NewTurnFragmentArgs.fromBundle(getArguments()).getIsReadOnly();
        btnSetTurn = view.findViewById(R.id.btnSetTurn);
        tilTurnDescription = view.findViewById(R.id.tilTurnDescription);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(NewTurnViewModel.class);

        btnSetTurn.setOnClickListener(this::onFABSetTurnClicked);

        LiveData<TurnResponse> turn = mViewModel.getTurnById(idTurn);
        turn.observe(getViewLifecycleOwner(),this::onTurnChanged);
    }

    private void onTurnChanged(TurnResponse turnResponse) {
        if(isReadOnly) {
            tilTurnDescription.getEditText().setEnabled(false);
            tilTurnDescription.getEditText().setText(turnResponse.getDescription());
            tilTurnDescription.setHint(getString(R.string.data_shared_by_your_student));
            btnSetTurn.setVisibility(View.GONE);
        }
    }

    private void onFABSetTurnClicked(View view) {
        String time = mViewModel.getTime();
        String desc = tilTurnDescription.getEditText().getText().toString().trim();
        mViewModel.insertTurn(email,idSubject,time,desc);

        NavDirections action = NewTurnFragmentDirections.actionNewTurnFragmentToTurnFragment(idSubject, email);
        Navigation.findNavController(view).navigate(action);
    }

}
