package com.itb.itsmyturn.ui.login;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import com.itb.itsmyturn.R;
import com.itb.itsmyturn.model.AuthToken;
import com.itb.itsmyturn.model.Login;
import com.itb.itsmyturn.utils.Constants;

import static android.content.Context.MODE_PRIVATE;

public class LoginFragment extends Fragment {

    private static final String AUTHTOKEN = "A1B2C3D4E5";
    private TextInputLayout tilLEmail;
    private TextInputLayout tilLPassword;
    private LoginViewModel mViewModel;
    private MaterialButton btnLLogin;
    private MaterialButton btnLRegister;
    private String email;
    private String password;
    private boolean isTeacher;
    private LiveData<Boolean> ldIsTeacher;
    private SharedPreferences sp;
    private SharedPreferences.Editor ed;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tilLEmail = view.findViewById(R.id.tilLEmail);
        tilLPassword = view.findViewById(R.id.tilLPassword);
        btnLLogin = view.findViewById(R.id.btnLLogin);
        btnLRegister = view.findViewById(R.id.btnLRegister);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);

        btnLLogin.setOnClickListener(this::onViewClicked);
        btnLRegister.setOnClickListener(this::onViewClicked);
        sp = this.getActivity().getSharedPreferences(Constants.LOGIN, MODE_PRIVATE);
        isTeacher = sp.getBoolean(Constants.IS_TEACHER, false);
        alreadyLogged();
    }

    private void onIsTeacherChanged(Boolean role) {
        boolean old = isTeacher;
        isTeacher = role;

        ed = sp.edit();
        ed.putBoolean(Constants.IS_TEACHER, isTeacher);
        ed.apply();

        //Per canviar el tema s'ha de recrear l'activitat pero dona problemes
        //if(old!=isTeacher) this.getActivity().recreate();

    }

    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLLogin:
                verificateLogin();
                break;
            case R.id.btnLRegister:
                Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_registerFragment);
                break;
        }
    }

    private void verificateLogin() {
        boolean validate = true;
        tilLEmail.setError("");
        tilLPassword.setError("");
        validate = validateNotEmpty(tilLEmail, validate);
        validate = validateNotEmpty(tilLPassword, validate);
        if (validate){
            email = tilLEmail.getEditText().getText().toString();
            password = tilLPassword.getEditText().getText().toString();
            Login login = new Login(email,password);
            ldIsTeacher = mViewModel.isTeacher(email);
            ldIsTeacher.observe(getViewLifecycleOwner(), this::onIsTeacherChanged);
            LiveData<AuthToken> authTokenLiveData = mViewModel.Login(login);
            authTokenLiveData.observe(this, this::onLoginResponse);
        }
    }

    private void onLoginResponse(AuthToken authToken) {
        if (authToken.getAuth().equals(AUTHTOKEN)){
            ed = sp.edit();
            ed.putString(Constants.KEY_EMAIL, email);
            ed.putBoolean(Constants.IS_LOGGED,true);
            ed.putBoolean(Constants.IS_TEACHER, isTeacher);
            ed.apply();
            NavDirections action = LoginFragmentDirections.actionLoginFragmentToClassesFragment(email);
            Navigation.findNavController(getView()).navigate(action);
        }else{
            Toast.makeText(getContext(), getString(R.string.incorrect_email_or_password),Toast.LENGTH_LONG).show();
        }
    }

    private boolean validateNotEmpty(TextInputLayout layout, boolean validate) {
        if (layout.getEditText().getText().toString().isEmpty()) {
            layout.setError(getString(R.string.required_field));
            scrollTo(layout);
            validate = false;
        }
        return validate;
    }
    private void scrollTo(TextInputLayout targetView) {
        targetView.getParent().requestChildFocus(targetView, targetView);
    }

    private void alreadyLogged(){
        if(sp.getBoolean(Constants.IS_LOGGED, false)) {
            email = sp.getString(Constants.KEY_EMAIL, null);
            ldIsTeacher = mViewModel.isTeacher(email);
            ldIsTeacher.observe(getViewLifecycleOwner(), this::onIsTeacherChanged);

            NavDirections action = LoginFragmentDirections.actionLoginFragmentToClassesFragment(email);
            Navigation.findNavController(getView()).navigate(action);
        }
    }
}
