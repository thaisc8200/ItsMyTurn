package com.itb.itsmyturn.ui.turn;

import com.itb.itsmyturn.model.TurnResponse;
import com.itb.itsmyturn.repository.Repository;

import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

public class NewTurnViewModel extends ViewModel {
    private Repository repository = new Repository();

    public String getTime(){
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        return df.format(date);
    }

    public LiveData<String> insertTurn(String email, int idSubject, String time, String description){
        return repository.insertTurn(email, idSubject, time, description);
    }

    public LiveData<TurnResponse> getTurnById(int idTurn){
        return repository.getTurnById(idTurn);
    }

}
