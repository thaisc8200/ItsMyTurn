package com.itb.itsmyturn.ui.classes;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.itb.itsmyturn.R;
import com.itb.itsmyturn.model.Subject;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ClassesAdapter extends RecyclerView.Adapter<ClassesAdapter.TurnViewHolder> {
    private List<Subject> classes;
    private OnClassesClickListener onClassesClickListener;
    private boolean isTeacher;

    public ClassesAdapter() {
    }

    public void setClasses(List<Subject> classes) {
        this.classes = classes;
        notifyDataSetChanged();
    }

    public void setIsTeacher(boolean teacher) {
        isTeacher = teacher;
    }

    public void setOnClassesClickListener(OnClassesClickListener onTurnClickListener) {
        this.onClassesClickListener = onTurnClickListener;
    }

    @NonNull
    @Override
    public TurnViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.classe_item, parent, false);
        return new TurnViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TurnViewHolder holder, int position) {
        Subject classe = classes.get(position);
        holder.tvSubject.setText(classe.getSubjectName());
        holder.tvClass.setText(classe.getUserGroup());
        if(isTeacher){
            holder.tvPassword.setText(String.valueOf(classe.getId()));
            holder.tvPassword.setVisibility(View.VISIBLE);
        } else {
            holder.tvPassword.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        int size = 0;
        if(classes != null) size = classes.size();
        return size;
    }

    public class TurnViewHolder extends RecyclerView.ViewHolder{
        TextView tvSubject;
        TextView tvClass;
        TextView tvPassword;

        public TurnViewHolder(@NonNull View view) {
            super(view);

            tvSubject = view.findViewById(R.id.tvSubjectNameText);
            tvClass = view.findViewById(R.id.tvClassNameText);
            tvPassword = view.findViewById(R.id.tvClassPasswordText);
            view.setOnClickListener(this::onClasseClicked);
        }
        public void onClasseClicked(View view){
            Subject turn = classes.get(getAdapterPosition());
            onClassesClickListener.onClasseClicked(turn);
        }
    }

    public interface OnClassesClickListener{
        void onClasseClicked(Subject classe);
    }
}
