package com.itb.itsmyturn.ui.turn;

import com.itb.itsmyturn.model.TurnList;
import com.itb.itsmyturn.repository.Repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

public class TurnViewModel extends ViewModel {
    private Repository repository = new Repository();

    public LiveData<Boolean> isTeacher(String email){
        return repository.isTeacher(email);
    }

    public LiveData<TurnList> getTurnsByClasse(int id){
        return repository.getTurnsByClasse(id);
    }

    public LiveData<String> deleteTurn(int idTurn){
        return repository.deleteTurn(idTurn);
    }
}
