package com.itb.itsmyturn;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;

import com.itb.itsmyturn.utils.Constants;

public class MainActivity extends AppCompatActivity {
    SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sp = getSharedPreferences(Constants.LOGIN, MODE_PRIVATE);
        if(sp.getBoolean(Constants.IS_TEACHER, false)){
            setTheme(R.style.TeacherTheme);
        } else{
            setTheme(R.style.StudentTheme);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
}
